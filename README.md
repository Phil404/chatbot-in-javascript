# Chatbot in JavaScript
Dieses Projekt soll als Grundlage für einen Chatbot dienen.  
[Hier findest du eine passende Präsentation dazu.](https://slides.com/phil404/deck)

## Was ist die Grundausstattung für einen Chat?
- [ ] Eine Eingabemöglichkeit für eine Nachricht
- [ ] Diese Nachricht soll in einer Art Chatfenster zu sehen sein

## Was ist die Grundausstattung für den Chatbot?
- [ ] Die Nachricht von dem Nutzer wird verarbeitet
- [ ] Der Bot reagiert auf die Nachricht
- [ ] Die Nachricht vom Bot wird als Antwort im Chatfenster dargestellt

## Anleitungen
### Abfangen der Formular Versands
```
// Definierung einer Funktion um eine alternative Aktion auszuführen.
function method(e) {
  // Mach etwas
  e.preventDefault();
}

// Hänge die Funktion an das Formular mit der ID 'message-box'
document.getElementById('message-box').addEventListener('submit', method, false);
```

### Wetter API Schnittstelle
Für die Wetter Auskunft für einen gewissen Standort wird die kostenlose API https://openweathermap.org genutzt.  
Mit dieser kann man alle vorliegenden Wetter Informationen bezüglich des Standortes abfragen.  
Man kann die Wetterdaten wie folgt anfragen:  
`http://api.openweathermap.org/data/2.5/weather?q={Stadt},{Land}&APPID={API_SCHLÜSSEL}&mode={MODUS}&units=metric`  
Mehr Informationen findet man in der Dokumentation der API: https://openweathermap.org/current  

```
var request = new XMLHttpRequest();
var url = 'FÜGE URL EIN';

request.open('GET', url, true);
request.onload = function() {
  var data = JSON.parse(this.response)

  if (request.status == 200) {
    console.log(data);
  } else {
    console.log('error');
  }
}

request.send();
```